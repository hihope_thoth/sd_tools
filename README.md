# Thoth mini烧录指南



## 环境安装

##### 下载镜像

```
wget https://repo.huaweicloud.com/ubuntu-cdimage/releases/18.04.5/release/ubuntu-18.04.6-server-arm64.iso
```





##### 安装工具链

```
apt-get install -y python make gcc unzip bison flex libncurses-dev squashfs-tools bc
```

```
apt-get install -y qemu-user-static binfmt-support gcc-aarch64-linux-gnu g++-aarch64-linux-gnu
```



## 烧录镜像

1. 将sd卡读卡器插入电脑usb，确认烧录接口

2. 通过烧录脚本进行sd卡烧录

   命令格式:
   ```
	python3 make_sd_card.py local [SD Name]
   ```
   例：

	```
	python3 make_sd_card.py local /dev/sdb
	```

